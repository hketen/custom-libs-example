
How ever to be able to get it up and running one must know the following
1. The custom package need to be referenced from the app/package.json and
2. even more importantly, from package-lock.json with `"version": "file:libs"` annotation!
3. The package.json must contain a "main" entry which references one of the files inside and its path must start with `./`!!!

You can test it with: 
- Create symlink `$ npm install` 
- Start example `$ node index.js` 

and verify with:
``` 
$ npm install // only once
$ node 
> module.paths
[
  '.../important-code-snippets/JavaScript/require-custom-assets/repl/node_modules',
  '.../important-code-snippets/JavaScript/require-custom-assets/node_modules',

```
